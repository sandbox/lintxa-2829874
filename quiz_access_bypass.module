<?php
/**
* Implements hook_menu().
*/
function quiz_access_bypass_menu() {
  $items['bypass-user'] = array(
    'title' => t('Bypass quiz users'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('quiz_access_bypass_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer access control'),
    'description' => t('Bypass closed quiz'),
    'type' =>  MENU_NORMAL_ITEM,
    'menu_name' => 'management',
    'weight' => 50,
  );
  $items['emails/autocomplete'] = array(
    'title' => t('AutoComp Menu'),
    'page callback' => 'selectAllEmails',
    'access callback' => 'user_access',
    'access arguments' => array('administer access control'),
    'type' => MENU_CALLBACK,
  );
  $items['courses/autocomplete'] = array(
    'title' => t('AutoComp Menu'),
    'page callback' => 'selectAllCourses',
    'access callback' => 'user_access',
    'access arguments' => array('administer access control'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
* Page callback quiz_access_bypass_form.
*/
function quiz_access_bypass_form($form_state) {
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Select user and quiz.'),
  );
  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#required' => TRUE,
    '#maxlength' => 128,
    '#autocomplete_path' => 'emails/autocomplete',
  );
  $form['course'] = array(
    '#type' => 'textfield',
    '#title' => t('Quiz Name'),
    '#required' => TRUE,
    '#maxlength' => 128,
    '#autocomplete_path' => 'courses/autocomplete',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

function selectAllEmails($string) {
  $result = db_query("SELECT mail FROM {users} WHERE mail LIKE '%$string%'");
  $matches = array();
  foreach($result as $item) {
    $matches[$item->mail] = check_plain($item->mail);
  }
  drupal_json_output($matches);
  exit;
}

function selectAllCourses($string) {
  $result = db_query("SELECT title, nid FROM {node} WHERE type LIKE 'quiz' AND title LIKE '%$string%'");
  $matches = array();
  foreach($result as $item) {
    $matches[$item->title] = check_plain($item->title);
  }
  drupal_json_output($matches);
  exit;
}

/**
* Validate quiz_access_bypass_form.
*/
function quiz_access_bypass_form_validate($form, &$form_state) {
  if (empty($form_state['values']['email']) || empty($form_state['values']['course'])) {
    form_set_error();
  }
  // Gets uid:
  $uid = quiz_access_bypass_getnids('users', 'mail', $form_state['values']['email'], 'uid');
  // Gets quiz id:
  $quiz_id = quiz_access_bypass_getnids('node', 'title', $form_state['values']['course'], 'nid');
}

/**
* Submit quiz_access_bypass_form.
*/
function quiz_access_bypass_form_submit($form, &$form_state) {
  // Gets uid:
  $uid = quiz_access_bypass_getnids('users', 'mail', $form_state['values']['email'], 'uid');
  // Gets quiz id:
  $quiz_id = quiz_access_bypass_getnids('node', 'title', $form_state['values']['course'], 'nid');
  // Inserting data:
  $result = db_insert('quiz_access_bypass')
              ->fields(array(
                'uid' => $uid,
                'course' => $quiz_id,
              ))
              ->execute();
  drupal_set_message (t('<strong>User added to bypass.</strong>'));
}

/**
 * Return id
 *
 * @param string $table Table to consult
 * @param string $field search field name
 * @param string $f_item String to find
 * @param string $f_nid field name of id
 *
 * @return int
 */
function quiz_access_bypass_getnids($table, $field, $f_item, $f_nid) {
  $result = db_select($table, 't')
    ->fields('t')
    ->condition($field, $f_item, '=')
    ->execute()
    ->fetchAssoc();
  return $result{$f_nid};
}

/**
 * Implements hook_quiz_availability_alter().
 *
 * @deprecated
 */
function quiz_access_bypass_quiz_availability_alter(&$return, $quiz) {
  global $user;

  if (quiz_access_bypass_is_bypass($user->uid, $quiz->nid) >= 1) {
    $quiz->quiz_always = 1;
    $return = TRUE;
  }
}

/**
 * Implements hook_quiz_access_alter().
 */
function quiz_access_bypass_quiz_access_alter(&$hooks, $op, $quiz, $account) {
  global $user;
  if (quiz_access_bypass_is_bypass($user->uid, $quiz->nid) >= 1) {
    $quiz->quiz_always = 1;
    $return = TRUE;
  }
}

/**
 * Check if the user is on the authorized user list.
 *
 * @param int $uid User ID
 * @param int $quiz Quiz ID
 *
 * @return int
 */
function quiz_access_bypass_is_bypass( $uid, $quiz) {
  $result = db_select('quiz_access_bypass', 'b')
    ->fields('b')
    ->condition('uid', $uid, '=')
    ->condition('course', $quiz, '=')
    ->execute();
  return $result->rowCount();
}
